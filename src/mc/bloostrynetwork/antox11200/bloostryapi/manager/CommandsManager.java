package mc.bloostrynetwork.antox11200.bloostryapi.manager;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.exceptions.PluginNotFoundException;

public class CommandsManager implements CommandExecutor {

	private JavaPlugin instance;
	public HashMap<JavaPlugin, ArrayList<Object>> commands = new HashMap<>();

	public CommandsManager(JavaPlugin instance) {
		this.instance = instance;
	}

	public void registerCommands() {
		Console.sendMessageDebug("CommandsManager.registerCommands() handled !");
		for (JavaPlugin instances : commands.keySet()) {
			ArrayList<Object> objects = commands.get(instances);
			for (Object object : objects) {
				ICommand cmd = (ICommand) object;
				PluginCommand pluginCmd = instances.getCommand(cmd.getCommand());
				if (cmd.getTabCompleter() != null) {
					pluginCmd.setTabCompleter(cmd.getTabCompleter());
					Console.sendMessageDebug(cmd.getTabCompleter()+"");
				}
				pluginCmd.setExecutor(this);
				Console.sendMessageDebug("/" + cmd.getCommand() + " is executable for plugin " + instance.getName());
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		String cmdName = cmd.getName();
		Console.sendMessageDebug(cmdName + " is executed by " + sender.getName());
		for (JavaPlugin instances : commands.keySet()) {
			Console.sendMessageDebug("JavaPlugin instances = " + instances.getName());
			ArrayList<Object> objects = commands.get(instances);
			for (Object object : objects) {
				ICommand iCmd = (ICommand) object;
				Console.sendMessageDebug(cmdName + " -> " + iCmd.getCommand());
				if (cmdName.equalsIgnoreCase(iCmd.getCommand())) {
					iCmd.handle(sender, args);
					Console.sendMessageDebug("/" + iCmd.getCommand() + " is handled by " + sender.getName()
							+ " for class ICommand " + object.getClass().getPackage() + "." + object.getClass());
				}
			}
		}
		return false;
	}

	public void addCommand(JavaPlugin instance, ICommand command) {
		ArrayList<Object> cmds = commands.get(instance);
		if (cmds == null) {
			cmds = new ArrayList<>();
		}
		cmds.add(command);
		commands.put(instance, cmds);
		Console.sendMessageDebug("The command " + command.getCommand() + " is added from list commands !");
	}

	public void removeCommand(JavaPlugin instance, ICommand command) {
		ArrayList<Object> cmds = commands.get(instance);

		new PluginNotFoundException(instance.getName());

		if (cmds == null) {
			return;
		}
		cmds.remove(command);
		commands.put(instance, cmds);
		Console.sendMessageDebug("The command " + command.getCommand() + " is removed from list commands !");
	}

	public JavaPlugin getInstance() {
		return this.instance;
	}
}

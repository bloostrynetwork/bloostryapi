package mc.bloostrynetwork.antox11200.bloostryapi.manager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.listeners.AsyncPlayerPreprocessListener;
import mc.bloostrynetwork.antox11200.bloostryapi.listeners.PlayerChatListener;
import mc.bloostrynetwork.antox11200.bloostryapi.listeners.PlayerConnectionListener;

public class EventsManager {

	public void registerListeners() {
		JavaPlugin plugin = Main.javaPlugin;
		PluginManager pluginManager = Bukkit.getPluginManager();
		pluginManager.registerEvents(new AsyncPlayerPreprocessListener(), plugin);
		pluginManager.registerEvents(new PlayerChatListener(), plugin);
		pluginManager.registerEvents(new PlayerConnectionListener(), plugin);
	}
}

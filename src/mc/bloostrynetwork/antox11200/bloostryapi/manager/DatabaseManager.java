package mc.bloostrynetwork.antox11200.bloostryapi.manager;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.mysql.MySQLClient;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.redis.RedisClient;
import redis.clients.jedis.Jedis;

public class DatabaseManager {
	
	private RedisClient redisClient;
	private MySQLClient mysqlClient;

	//Permet de lancer la connexion de la base de donn�e (redis)
	public void connectionRedis(String name, String adress, int port, String password) {
		this.redisClient = new RedisClient(name, adress, port, password);
		this.redisClient.initializedConnection();
	}

	//Permet de lancer la connexion de la base de donn�e (mysql)
	public void connectionMySQL(String host, String database, String user, String password) {
		this.mysqlClient = new MySQLClient("jdbc:mysql://", host, database, user, password);
		this.mysqlClient.connection();	
	}

	//Permet de d�connecter toutes les base de donn�es (redis/mysql)
	public void deconnectionAllDatabases() {
		if(this.redisClient != null) {
			this.redisClient.deconnection();
			this.redisClient.cancelTask();
		}
		if(this.mysqlClient != null)
			this.mysqlClient.deconnection(); 
	}
	
	public RedisClient getRedisClient() {
		return this.redisClient;
	}
	
	public MySQLClient getMySQLClient() {
		return this.mysqlClient;
	}
}

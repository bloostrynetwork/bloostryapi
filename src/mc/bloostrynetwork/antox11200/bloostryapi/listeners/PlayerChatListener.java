package mc.bloostrynetwork.antox11200.bloostryapi.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.TimeUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.DatabaseServerType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.BloostryRank;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class PlayerChatListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerChatEvent_(AsyncPlayerChatEvent e) {
		if (e.isCancelled())
			return;

		Player player = e.getPlayer();
		
		BloostryPlayer bloostryPlayerTest = DatabaseServerType.REDIS.getRequestDatabase().getData(player.getUniqueId());
		if(bloostryPlayerTest != null) {
			Console.sendMessageDebug("�aYes");
		}

		String message = e.getMessage();
		if (message.contains("%")) {
			player.sendMessage("�cVous ne pouvez pas mettre de signe �e'%' �c!");
			e.setCancelled(true);
		}

		String newMessage = null;

		if (Main.bloostryAPI.getDatabaseManager().getRedisClient().isConnected()) {
			BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(player.getUniqueId());

			if (iplayer.isMuted() != null) {
				e.setCancelled(true);
				Sanction sanction = iplayer.isMuted();
				if (sanction.getSanctionType() == SanctionType.TEMPORARY) {
					iplayer.sendMessage(BloostryAPI.PREFIX + "�cVous �tes mute temporairement par �f"
							+ sanction.getAuthor() + " �cpour �7'�f" + sanction.getReason() + "�7' �cpendant �f"
							+ (TimeUtils.getMSG(sanction.getTimeLeft())));
				} else {
					iplayer.sendMessage(BloostryAPI.PREFIX + "�cVous �tes mute par �f" + sanction.getAuthor()
							+ " �cpour �7'�f" + sanction.getReason()+"�7'");
				}
			}

			BloostryRank rank = iplayer.getBloostryRank();
			newMessage = rank.getPrefix() + player.getName() + " �7: " + rank.getChatColor() + message;
			if(rank.getPermissionPower() >= Ranks.VIP.getBloostryRank().getPermissionPower()) {
				newMessage.replace('&', '�');
			}
		} else {
			newMessage = "�8[�cnull�8] �7" + player.getName() + " �7: " + message;
		}
		
		e.setFormat(newMessage);
	}
}

package mc.bloostrynetwork.antox11200.bloostryapi.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;

public class AsyncPlayerPreprocessListener implements Listener {

	@EventHandler (priority = EventPriority.LOWEST)
	public void chatCommand(PlayerCommandPreprocessEvent e) {
		String cmd = e.getMessage().split(" ")[0];
		List<String> commandBlacklisted = new ArrayList<>();
		HashMap<String, String> editCommand = new HashMap<>();
		commandBlacklisted.add("/minecraft:say");
		commandBlacklisted.add("/minecraft:me");
		commandBlacklisted.add("/me");
		commandBlacklisted.add("/say");
		commandBlacklisted.add("/worldedit:/calc");
		commandBlacklisted.add("//calc");

		editCommand.put("/say", "uhchost:say");
		editCommand.put("/minecraft:say", "uhchost:say");
		
		for(String command : commandBlacklisted) {
			if(command.equalsIgnoreCase("/reload") || command.equalsIgnoreCase("/rl")) {
				Main.javaPlugin.getConfig().set("reload", true);
			}
			if(command.equalsIgnoreCase(cmd)) {
				e.setCancelled(true);
				if(editCommand.containsKey(command)) {
					Bukkit.dispatchCommand(e.getPlayer(), editCommand.get(command));
				} else {
					e.getPlayer().sendMessage("�cVous ne pouvez pas �x�cuter cette commande !");
				}
				return;
			}
		}
	}
}
package mc.bloostrynetwork.antox11200.bloostryapi.listeners;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.TimeUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.DatabaseServerType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.RequestDatabase;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class PlayerConnectionListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerLoginEvent_(PlayerLoginEvent e) {
		if (e.getAddress().getHostAddress().equals("127.0.0.1")
				|| e.getAddress().getHostAddress().equals("localhost")) {
			e.disallow(Result.KICK_BANNED,
					"�7D�connexion du serveur \n�Le syst�me de connexion que vous utilisez n'est pas accepter sur notre serveur :/");
			return;
		}

		Player player = e.getPlayer();

		UUID UUID = player.getUniqueId();

		String uuid = UUID.toString();
		String name = player.getName();

		RequestDatabase requesterDatabaseMySQL = DatabaseServerType.MYSQL.getRequestDatabase();
		RequestDatabase requesterDatabaseRedis = DatabaseServerType.REDIS.getRequestDatabase();

		BloostryPlayer bloostryPlayer = null;

		if (requesterDatabaseMySQL.hasUser(name)) {
			// LE NOM DU JOUEUR EXISTE
			if (requesterDatabaseMySQL.hasData(UUID)) {
				// LE COMPTE DU JOUEUR EXISTE
				bloostryPlayer = requesterDatabaseMySQL.getData(UUID);
			} else {
				// LE COMPTE DU JOUEUR EXISTE PAS DONC ON LUI EN CREER UN
				requesterDatabaseMySQL.createData(UUID, new BloostryPlayer(uuid, name));
				bloostryPlayer = requesterDatabaseMySQL.getData(UUID);
				Console.sendMessageDebug("Created account on database mysql ");
			}
		} else {
			// LE NOM DU JOUEUR EXISTE PAS
			if (requesterDatabaseMySQL.hasData(UUID)) {
				// LE COMPTE DU JOUEUR EXISTE ET DONC ON DEDUIS QUE LE JOUEUR A CHANGER DE
				// PSEUDO
				bloostryPlayer = requesterDatabaseMySQL.getData(UUID);

				Console.sendMessageDebug("Edit last name (" + bloostryPlayer.getName() + ") for new name " + player.getName());

				// SUPPRESION DE L'ANCIEN PSEUDONYME
				requesterDatabaseMySQL.deleteUser(bloostryPlayer.getName());

				// DEFINITION DU NOUVEAU PSEUDONYME
				requesterDatabaseMySQL.createUser(name, UUID);

				// DEFINITION DU NOUVEAU PSEUDONYME SUR BLOOSTRYPLAYER
				bloostryPlayer.setName(name);

				bloostryPlayer = requesterDatabaseMySQL.getData(UUID);

			} else {
				// LE JOUEUR CE CONNECTE POUR LA PREMIERE FOIS AVEC CE COMPTE
				requesterDatabaseMySQL.createUser(name, UUID);
				requesterDatabaseMySQL.createData(UUID, new BloostryPlayer(uuid, name));
				bloostryPlayer = requesterDatabaseMySQL.getData(UUID);
				Console.sendMessageDebug("Created account and user on database mysql");
			}
		}

		requesterDatabaseRedis.setData(UUID, bloostryPlayer);

		if(e.getResult() == Result.KICK_WHITELIST) {
			if(bloostryPlayer.getBloostryRank().getPermissionPower() >= Ranks.MYSTIC.getBloostryRank().getPermissionPower()) {
				e.allow();
				return;
			}
			e.disallow(Result.KICK_WHITELIST, "�7D�connexion du serveur \n�cCe serveur est actuellement en maintenance, veuillez r�-essayer plus tard.");
		}

		Main.bloostryAPI.getDatabaseManager().getRedisClient().addPlayerOnline(BloostryAPI.getServerName());

		Sanction sanction = bloostryPlayer.isBanned();
		// VERIFICATION SI LE JOUEUR POSSEDE UNE SANCTION (dont si il est banni)
		if (sanction != null) {
			if (sanction.getSanctionType() == SanctionType.PERMANENT) {
				// CE JOUEUR A UNE SANCTION PERMANENT
				e.disallow(Result.KICK_BANNED, "�cVous �tes banni du serveur BloostryNetwork \n�cAuteur: �f"
						+ sanction.getAuthor() + " \n�CRaison: �F" + sanction.getReason() + " \n�cPendant: �fPermanent"
						+ "\n�6 \n�7Si vous voulez contester votre bannisement veuillez vous rendre sur notre teamspeak \n�ats.bloostrynetwork.fr");
			} else {
				// CE JOUEUR A UNE SANCTION TEMPORAIRE
				e.disallow(Result.KICK_BANNED, "�cVous �tes banni du serveur BloostryNetwork \n�cAuteur: �f"
						+ sanction.getAuthor() + " \n�CRaison: �F" + sanction.getReason() + " \n�cPendant: �f"
						+ (TimeUtils.getMSG(sanction.getTimeLeft()))
						+ "\n�6 \n�7Si vous voulez contester votre bannisement veuillez vous rendre sur notre teamspeak \n�ats.bloostrynetwork.fr");
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerJoinEvent_(PlayerJoinEvent e) {
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerQuitEvent_(PlayerQuitEvent e) {
		Player player = e.getPlayer();

		UUID UUID = player.getUniqueId();

		String uuid = UUID.toString();
		String name = player.getName();

		RequestDatabase requesterDatabaseMySQL = DatabaseServerType.MYSQL.getRequestDatabase();
		RequestDatabase requesterDatabaseRedis = DatabaseServerType.REDIS.getRequestDatabase();

		BloostryPlayer bloostryPlayer = requesterDatabaseRedis.getData(UUID);
		if (bloostryPlayer != null) {
			bloostryPlayer.setLastConnecion(System.currentTimeMillis());
		} else {
			bloostryPlayer = requesterDatabaseMySQL.getData(UUID);
			bloostryPlayer.setLastConnecion(System.currentTimeMillis());
			Console.sendMessageError("�cPlayerData recovered with Redis not succes !");
			Console.sendMessageInfo("�cSet last connection PlayerData on database mysql");
		}

		if (requesterDatabaseMySQL.hasData(name)) {
			requesterDatabaseMySQL.setData(UUID, bloostryPlayer);
		} else {
			requesterDatabaseMySQL.createData(UUID, bloostryPlayer);
		}

		Console.sendMessageDebug(
				"Update Data BloostyPlayer on MySQL database and delete " + uuid + " on redis database");
		Main.bloostryAPI.getDatabaseManager().getRedisClient().removePlayerOnline(BloostryAPI.getServerName());
	}
}

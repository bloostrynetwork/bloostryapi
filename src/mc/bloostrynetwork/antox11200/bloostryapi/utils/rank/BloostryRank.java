package mc.bloostrynetwork.antox11200.bloostryapi.utils.rank;

public class BloostryRank {

	private String name;
	private String nameColered;
	private String prefix;
	private String chatColor;
	private Long permissionPower;
	private String teamName;
	private int id;
	
	public BloostryRank(String name, String nameColered, String prefix, String chatColor, Long permissionPower, String teamName, int id) {
		this.name = name;
		this.nameColered = nameColered;
		this.prefix = prefix;
		this.chatColor = chatColor;
		this.permissionPower = permissionPower;
		this.teamName = teamName;
		this.id = id;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getName() {
		return name;
	}

	public String getNameColered() {
		return nameColered;
	}

	public Long getPermissionPower() {
		return permissionPower;
	}

	public String getTeamName() {
		return teamName;
	}

	public int getId() {
		return id;
	}

	public String getChatColor() {
		return chatColor;
	}

	public void setChatColor(String chatColor) {
		this.chatColor = chatColor;
	}
}

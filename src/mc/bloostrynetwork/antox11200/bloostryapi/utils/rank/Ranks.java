package mc.bloostrynetwork.antox11200.bloostryapi.utils.rank;

import java.util.HashMap;

public enum Ranks {

	ADMINISTRATOR(new BloostryRank("admin", "�cAdmin", "�8[�cAdmin�8] �c", "�f", 10000L, "aAdministrator", 1)),
	RESPONSIBLE(new BloostryRank("responsable", "�5Responsable", "�8[�5Responsable�8] �5", "�f", 1000L, "bResponsible", 2)),
	RESP_DEVELOPER(new BloostryRank("resp.developpeur", "�9Resp.D�veloppeur", "�8[�9Resp.D�veloppeur�8] �9", "�f", 950L, "cRespDevelopor", 3)),
	DEVELOPER(new BloostryRank("developpeur", "�9D�veloppeur", "�8[�9D�veloppeur�8] �9", "�f", 900L, "dDevelopor", 4)),
	RESP_MODERATOR(new BloostryRank("resp.moderateur", "�aResp.Mod�rateur", "�8[�aResp.Mod�rateur�8] �9", "�f", 750L, "cRespModerator", 5)),
	MODERATOR(new BloostryRank("moderateur", "�aMod�rateur", "�8[�aMod�rateur�8] �a", "�f", 700L, "eModerator", 6)),
	HOST(new BloostryRank("host", "�6Host", "�8[�6Host�8] �6", "�f", 600L, "fHost", 7)),
	HELPER(new BloostryRank("helper", "�3Helper", "�8[�3Helper�8] �3", "�f", 500L, "gHelper", 8)),
	MYSTIC(new BloostryRank("mystic", "�BMystic", "�8[�bMystic�8] �b", "�f", 400L, "xMystic", 9)),
	VIP(new BloostryRank("vip", "�EVIP", "�8[�eVIP�8] �e", "�f", 300L, "yVIP", 10)),
	PLAYER(new BloostryRank("joueur", "�7Joueur", "�7", "�7", 100L, "zPlayer", 11));
	
	private BloostryRank bloostryRank;
	
	Ranks(BloostryRank bloostryRank){
		this.bloostryRank = bloostryRank;
	}
	
	public BloostryRank getBloostryRank() {
		return this.bloostryRank;
	}

	private static HashMap<Integer, Ranks> ranksId = new HashMap<>();
	
	static {
		for(Ranks rank : values()) {
			ranksId.put(rank.getBloostryRank().getId(), rank);
		}
	}
	
	public static Ranks getBloostryRank(int id) {
		return ranksId.get(id);
	}
	
	public static Ranks getBloostryRank(String name) {
		for(Ranks rank : values()) {
			if(rank.getBloostryRank().getName().equalsIgnoreCase(name)) {
				return rank;
			}
		}
		return null;
	}
}

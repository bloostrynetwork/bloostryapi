package mc.bloostrynetwork.antox11200.bloostryapi.utils.sanctions;

public enum SanctionType {

	PERMANENT(1),
	TEMPORARY(2);
	
	private int banID;
	
	SanctionType(int banID){
		this.banID = banID;
	}
	 
	 public int getID(){
		 return banID;
	 }
}
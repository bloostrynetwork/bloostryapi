package mc.bloostrynetwork.antox11200.bloostryapi.utils.sanctions;

public class Sanction {

	private PunishmentType punishmentType;
	private long date;
	private long for_;
	private long timeLeft;
	private String author;
	private String reason;

	private boolean etat = true;

	public Sanction(PunishmentType punishmentType, long timeLeft, String author, String reason) {
		this.punishmentType = punishmentType;
		this.author = author;
		this.timeLeft = timeLeft;
		this.date = System.currentTimeMillis();
		this.for_ = (timeLeft - date);
		this.author = author;
		this.reason = reason;
		this.etat = true;
	}

	public PunishmentType getPunishmentType() {
		return this.punishmentType;
	}

	public long getFor() {
		return for_;
	}

	public long getTimeLeft() {
		return this.timeLeft;
	}

	public long getDate() {
		return this.date;
	}

	public boolean getEtat() {
		return this.etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public String getAuthor() {
		return this.author;
	}

	public String getReason() {
		return this.reason;
	}

	public SanctionType getSanctionType() {
		return Sanction.getSanctionType(this.timeLeft);
	}

	public static SanctionType getSanctionType(long time) {
		return (time == -1 ? SanctionType.PERMANENT : SanctionType.TEMPORARY);
	}
}
package mc.bloostrynetwork.antox11200.bloostryapi.utils.sanctions;

public enum TimeUtils {

	MINUTE("m", 1), HOUR("h", 60), DAY("d", 1440), WEEK("w", 10080), MONTH("month", 43200), YEAR("y",
			518400);

	public String name;
	public int multi;

	private TimeUtils(String n, int mult) {
		this.name = n;
		this.multi = mult;
	}

	public static Long getTicks(String un, int time) {
		long sec;
		try {
			sec = time * 60;
		} catch (NumberFormatException ex) {
			return 0L;
		}
		TimeUtils[] arrayOfTimeUtils;
		int j = (arrayOfTimeUtils = values()).length;
		for (int i = 0; i < j; i++) {
			TimeUtils unit = arrayOfTimeUtils[i];
			if (un.startsWith(unit.name)) {
				return sec *= unit.multi * 1000;
			}
		}
		return null;
	}

	public static String getMSG(long endOfBan) {
		String message = "";
		int seconds = (int) ((endOfBan - System.currentTimeMillis()) / 1000L);
		if (seconds >= 86400) {
			int jours = seconds / 86400;
			seconds %= 86400;

			message = message + jours + " jour(s) ";
		}
		if (seconds >= 3600) {
			int hours = seconds / 3600;
			seconds %= 3600;

			message = message + hours + " heure(s) ";
		}
		if (seconds >= 60) {
			int min = seconds / 60;
			seconds %= 60;

			message = message + min + " minute(s) ";
		} else if (seconds != 0) {
			message = message + seconds + " seconde(s)";
		}
		return message;
	}

	public static String getTranslate(long value) {
		String message = "";
		int seconds = (int) ((value / 1000L));
		if (seconds >= 86400) {
			int jours = seconds / 86400;
			seconds %= 86400;

			message = message + jours + " jour(s) ";
		}
		if (seconds >= 3600) {
			int hours = seconds / 3600;
			seconds %= 3600;

			message = message + hours + " heure(s) ";
		}
		if (seconds >= 60) {
			int min = seconds / 60;
			seconds %= 60;

			message = message + min + " minute(s) ";
		} else if (seconds != 0) {
			message = message + seconds + " seconde(s)";
		}
		return message;
	}
}

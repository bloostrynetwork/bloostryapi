package mc.bloostrynetwork.antox11200.bloostryapi.utils.network;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BungeeCommunication {

	public static void sendBroadcast(String message) {
		Bukkit.broadcastMessage(message);
	}

	public static void kickPlayer(String playerName, String message) {
		Player player = Bukkit.getPlayer(playerName);
		if(player != null) {
			player.kickPlayer(message);
		}
	}
}

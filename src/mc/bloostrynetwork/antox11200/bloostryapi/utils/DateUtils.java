package mc.bloostrynetwork.antox11200.bloostryapi.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {
	
	public static String getTimerDate(Integer seconds){
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat df = null; 
		if(seconds >= 3600){
			df = new SimpleDateFormat("HH?mm!ss.");
		} else if (seconds >= 60){
			df = new SimpleDateFormat("mm!ss.");
		} else if (seconds >= 1){
			df = new SimpleDateFormat("ss.");
		} else {
			return null;
		}
		df.setTimeZone(tz);
		return df.format(new Date(seconds * 1000)).replace('?', 'h').replace('!', 'm').replace('.', 's');
	}

	public static String getTimerDate(Long finalTime) {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		SimpleDateFormat df = null; 
		if(finalTime / 1000 >= 3600){
			df = new SimpleDateFormat("HH?mm!ss.");
		} else if (finalTime / 1000 >= 60){
			df = new SimpleDateFormat("mm!ss.");
		} else if (finalTime / 1000 >= 1){
			df = new SimpleDateFormat("ss.");
		} else {
			return null;
		}
		df.setTimeZone(tz);
		return df.format(new Date(finalTime)).replace('?', 'h').replace('!', 'm').replace('.', 's');
	}
}

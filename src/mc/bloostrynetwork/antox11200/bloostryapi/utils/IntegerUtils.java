package mc.bloostrynetwork.antox11200.bloostryapi.utils;

public class IntegerUtils {
	
	public static Integer getValueOf(String value) {
		try {
			Integer intValue= Integer.valueOf(value);
			return intValue;
		} catch (Exception e) {}
		return null;
	}
	
	public static boolean isInteger(String value) {
		return getValueOf(value) == null ? false : true;
	}
}

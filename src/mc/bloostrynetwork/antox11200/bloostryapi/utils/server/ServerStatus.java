package mc.bloostrynetwork.antox11200.bloostryapi.utils.server;

public enum ServerStatus {

	OPEN("open"),
	CLOSED("closed");
	
	private String keyPath;
	
	ServerStatus(String keyPath) {
		this.keyPath = keyPath;
	}
	
	public String getKeyPath() {
		return this.keyPath;
	}
	
	public static ServerStatus getServerStatusType(String keyPath) {
		for(ServerStatus keys : ServerStatus.values()) {
			if(keys.getKeyPath().equalsIgnoreCase(keyPath)) {
				return keys;
			}
		}
		return null;
	}
}

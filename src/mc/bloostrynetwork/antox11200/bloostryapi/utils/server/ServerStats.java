package mc.bloostrynetwork.antox11200.bloostryapi.utils.server;

import java.text.DecimalFormat;

import mc.bloostrynetwork.antox11200.bloostryapi.runnables.LagRunnable;
import oshi.SystemInfo;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;

public class ServerStats {

	public static long getUsedMemoryRAM() {
		Runtime runtime = Runtime.getRuntime();
		int mb = 1048576;
		return (runtime.totalMemory() - runtime.freeMemory()) / mb;
	}

	public static long getMaximumMemoryRAM() {
		int mb = 1048576;
		return Runtime.getRuntime().maxMemory() / mb;
	}

	public static String getTPS() {
		return new DecimalFormat("#.##").format(LagRunnable.getTPS());
	}

	public static long getProcessorRead() {
		SystemInfo si = new SystemInfo();
		OperatingSystem os = si.getOperatingSystem();

		OSProcess processor = os.getProcess(os.getProcessId());

		long read = processor.getBytesRead();
		return read;
	}

	public static long getProcessorWritten() {
		SystemInfo si = new SystemInfo();
		OperatingSystem os = si.getOperatingSystem();

		OSProcess processor = os.getProcess(os.getProcessId());

		long written = processor.getBytesRead();
		return written;
	}
}

package mc.bloostrynetwork.antox11200.bloostryapi.utils.player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.PunishmentType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.DatabaseServerType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.BloostryRank;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class BloostryPlayer {

	private String uuid;
	private String name;
	
	private Long firstConnection;
	private Long lastConnection;
	private HashMap<SanctionType, List<Sanction>> sanctions = new HashMap<>();
	
	private Double mystiCoins;
	private Double bloostryCoins;
	
	private long timeLeftForRank;
	
	private Integer rankID;

	public BloostryPlayer(String uuid, String name) {
		this.uuid = uuid;
		this.name = name;
		this.firstConnection = System.currentTimeMillis();
		this.lastConnection = System.currentTimeMillis();
		this.mystiCoins = 0D;
		this.bloostryCoins = 200.0D;
		this.rankID = Ranks.PLAYER.getBloostryRank().getId();
		this.timeLeftForRank = -1; 
	}

	public void addBloostryCoins(double amount) {
		this.bloostryCoins += amount;
		refresh();
	}

	public void removeBloostryCoins(double amount) {
		double result = this.bloostryCoins - amount;

		if (result <= 0) {
			this.bloostryCoins = 0D;
		} else {
			this.bloostryCoins -= amount;
		}
		refresh();
	}

	public void addMystiCoins(double amount) {
		this.mystiCoins += amount;
		refresh();
	}

	public void removeMystiCoins(double amount) {
		double result = this.mystiCoins - amount;

		if (result <= 0) {
			this.mystiCoins = 0D;
		} else {
			this.mystiCoins -= amount;
		}
		refresh();
	}

	public double getMystiCoins() {
		return this.mystiCoins;
	}

	public double getBloostryCoins() {
		return this.bloostryCoins;
	}

	public void sendMessage(String message) {
		if (isOnline())
			getPlayer().sendMessage(message);
	}

	public HashMap<SanctionType, List<Sanction>> getSanctions() {
		return sanctions;
	}

	public List<Sanction> getSanctionList(SanctionType sanctionType) {
		if (sanctions.get(sanctionType) != null) {
			return sanctions.get(sanctionType);
		}
		return null;
	}

	public Sanction isBanned() {
		Sanction ssanction = null;
		for (SanctionType sanctionType : sanctions.keySet()) {
			for (Sanction sanction : sanctions.get(sanctionType)) {
				if (sanction.getPunishmentType() == PunishmentType.BAN) {
					if (sanction.getSanctionType() == SanctionType.PERMANENT) {
						if (sanction.getEtat())
							ssanction = sanction;
					} else if (sanction.getTimeLeft() - System.currentTimeMillis() >= 0) {
						if (sanction.getEtat())
							ssanction = sanction;
					}
				}
			}
		}
		return ssanction;
	}

	public Sanction isMuted() {
		Sanction ssanction = null;
		for (SanctionType sanctionType : sanctions.keySet()) {
			for (Sanction sanction : sanctions.get(sanctionType)) {
				if (sanction.getPunishmentType() == PunishmentType.MUTE) {
					if (sanction.getSanctionType() == SanctionType.PERMANENT) {
						if (sanction.getEtat())
							ssanction = sanction;
					} else if (sanction.getTimeLeft() - System.currentTimeMillis() >= 0) {
						if (sanction.getEtat())
							ssanction = sanction;
					}
				}
			}
		}
		return ssanction;
	}

	public void addSanction(SanctionType sanctionType, Sanction sanction) {
		List<Sanction> sanctionList = getSanctionList(sanctionType);
		if (sanctionList == null)
			sanctionList = new ArrayList<>();
		sanctionList.add(sanction);
		sanctions.put(sanctionType, sanctionList);
		refresh();
	}

	public void removeSanction(SanctionType sanctionType, Sanction sanction) {
		List<Sanction> sanctionList = getSanctionList(sanctionType);
		if (sanctionList == null)
			return;
		if (!sanctionList.contains(sanction))
			return;
		sanctionList.remove(sanction);
		sanctions.put(sanctionType, sanctionList);
		refresh();
	}
	
	public void setBloostryRank(BloostryRank bloostryRank) {
		this.rankID = bloostryRank.getId();
		refresh();
	}
	
	public void refresh() {
		DatabaseServerType.MYSQL.getRequestDatabase().setData(getUUID(), this);
		DatabaseServerType.REDIS.getRequestDatabase().setData(getUUID(), this);
		/*
		if(!(isOnline())) {
			DatabaseServerType.MYSQL.getRequestDatabase().setData(getUUID(), this);
			DatabaseServerType.REDIS.getRequestDatabase().setData(getUUID(), this);
		} else {
			if(DatabaseServerType.REDIS.getRequestDatabase().hasData(getUUID())) {
				DatabaseServerType.REDIS.getRequestDatabase().setData(getUUID(), this);
			} else {
				if(DatabaseServerType.MYSQL.getRequestDatabase().hasData(getUUID())) {
					DatabaseServerType.MYSQL.getRequestDatabase().setData(getUUID(), this);	
				} else {
					DatabaseServerType.MYSQL.getRequestDatabase().createData(getUUID(), this);
				}
			}	
		}*/
	}
	
	public void initializeRank() {
		if(this.timeLeftForRank == -1) {
			return;
		} else {
			if(this.timeLeftForRank - System.currentTimeMillis() <= 0) {
				BloostryRank lastRank = Ranks.getBloostryRank(rankID).getBloostryRank();
				this.rankID = Ranks.PLAYER.getBloostryRank().getId();
				this.timeLeftForRank = -1;
				sendMessage(BloostryAPI.PREFIX + "�CVotre grade �e"+lastRank.getName()+" �cvient d'expirer.");
			}
		}
	}

	public void setName(String name) {
		this.name = name;
		refresh();
	}
	
	public long getTimeLeftForRank() {
	return this.timeLeftForRank;	
	}
	
	public long getFirstConnection() {
		return firstConnection;
	}
	
	public long getLastConnection() {
		return lastConnection;
	}
	
	public void setLastConnecion(long lastConnection) {
		this.lastConnection = lastConnection;
		refresh();
	}
	
	public String getName() {
		return this.name;
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(name);
	}
	
	public boolean isOnline() {
		return getPlayer() != null;
	}
	
	public Object getCraftPlayer(){
		if(!isOnline())return null;
		try {
			String bukkitversion = Bukkit.getServer().getClass().getPackage()
	                .getName().substring(23);
	        Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit."
			        + bukkitversion + ".entity.CraftPlayer");
			try {
				Object handle = craftPlayer.getMethod("getHandle").invoke(getPlayer());
				return handle;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public BloostryRank getBloostryRank() {
		return Ranks.getBloostryRank(rankID).getBloostryRank();
	}

	public UUID getUUID() {
		return UUID.fromString(this.uuid);
	}
}

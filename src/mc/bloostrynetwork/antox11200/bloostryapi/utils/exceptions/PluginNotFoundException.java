package mc.bloostrynetwork.antox11200.bloostryapi.utils.exceptions;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;

public class PluginNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -763515558985318770L;

	public PluginNotFoundException(String pluginName){
		Console.sendMessageInfo("The plugin '"+pluginName+"' are not in list of commands !");
	}	
}

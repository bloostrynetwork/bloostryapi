package mc.bloostrynetwork.antox11200.bloostryapi.utils.database.mysql;

public class Section {

	private String key;
	private Section.Type valueType;
	private Object value;

	public Section(String key, Section.Type valueType, Object value) {
		this.key = key;
		this.valueType = valueType;
		this.value = value;
	}

	public String getKey() {
		return this.key;
	}

	public Object getValue() {
		return this.value;
	}
	
	public Section.Type getValueType(){
		return valueType;
	}
	
	public enum Type {
		STRING,
		INTEGER,
		BOOLEAN;
	}
}

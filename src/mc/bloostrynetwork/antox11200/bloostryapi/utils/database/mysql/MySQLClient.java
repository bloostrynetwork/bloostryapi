package mc.bloostrynetwork.antox11200.bloostryapi.utils.database.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.RequestDatabase;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;

public class MySQLClient extends RequestDatabase {

	private final String PREFIX = "�8[�bMySQL�8] �c";

	private Connection connection;
	private String urlbase, host, database, user, pass;

	public MySQLClient(String urlbase, String host, String database, String user, String pass) {
		this.urlbase = urlbase;
		this.host = host;
		this.database = database;
		this.user = user;
		this.pass = pass;
	}

	public void connection() {
		if (!isConnected()) {
			try {
				connection = DriverManager.getConnection(urlbase + host + "/" + database, user, pass);
				Console.sendMessageInfo(PREFIX + "�aMySQL client is connected from server mysql :D");
			} catch (SQLException e) {
				Console.sendMessageInfo(PREFIX + "�cConnection on server mysql failed !");
				e.printStackTrace();
			}
		}
	}

	public void deconnection() {
		if (isConnected()) {
			try {
				connection.close();
				Console.sendMessageInfo(PREFIX + "�aMySQL client is disconnected from server mysql \nGood bye :D");
			} catch (SQLException e) {
				Console.sendMessageInfo(PREFIX + "�cConnection on server mysql failed !");
				e.printStackTrace();
			}
		}
	}

	public boolean isConnected() {
		try {
			return connection != null && (!(connection.isClosed())) && connection.isValid(10);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String getString(String table, String sectionRecover, String stringRecorver, String recoverSection) {
		if (!isConnected())
			connection();
		;
		String returnString = null;
		try {
			PreparedStatement q = connection.prepareStatement(
					"SELECT " + recoverSection + " FROM " + table + " WHERE " + sectionRecover + " = ?");
			q.setString(1, stringRecorver);
			ResultSet rs = q.executeQuery();

			while (rs.next()) {

				returnString = rs.getString(recoverSection);
			}

			q.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnString;
	}

	public Integer getInteger(String table, String sectionRecover, String stringRecorver, String recoverSection) {
		if (!isConnected())
			connection();
		Integer returnInteger = null;
		try {
			PreparedStatement q = connection.prepareStatement(
					"SELECT " + recoverSection + " FROM " + table + " WHERE " + sectionRecover + " = ?");
			q.setString(1, stringRecorver);

			ResultSet rs = q.executeQuery();

			while (rs.next()) {
				returnInteger = rs.getInt(recoverSection);
			}

			q.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnInteger;
	}

	public Boolean getBoolean(String table, String sectionRecover, String stringRecorver, String recoverSection) {
		if (!isConnected())
			connection();
		Boolean returnBoolean = null;
		try {
			PreparedStatement q = connection.prepareStatement(
					"SELECT " + recoverSection + " FROM " + table + " WHERE " + sectionRecover + " = ?");
			q.setString(1, stringRecorver);

			ResultSet rs = q.executeQuery();

			while (rs.next()) {
				returnBoolean = rs.getBoolean(recoverSection);
			}

			q.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return returnBoolean;
	}

	public void setString(String table, String section, String sectionValue, String valueSection, String value) {
		if (!isConnected())
			connection();
		;
		try {
			PreparedStatement rs = connection.prepareStatement(
					"UPDATE " + table + " SET " + valueSection + " = ? WHERE " + section + " = '" + sectionValue + "'");
			rs.setString(1, value);
			rs.executeUpdate();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setInteger(String table, String section, String sectionValue, String valueSection, int value) {
		if (!isConnected())
			connection();
		;
		try {
			PreparedStatement rs = connection.prepareStatement(
					"UPDATE " + table + " SET " + valueSection + " = ? WHERE " + section + " = '" + sectionValue + "'");
			rs.setInt(1, value);
			rs.executeUpdate();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void setBoolean(String table, String section, String sectionValue, String valueSection, boolean value) {
		if (!isConnected())
			connection();
		;
		try {
			PreparedStatement rs = connection.prepareStatement(
					"UPDATE " + table + " SET " + valueSection + " = ? WHERE " + section + " = '" + sectionValue + "'");
			rs.setBoolean(1, value);
			rs.executeUpdate();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean has(String table, String section, String search) {
		if (!isConnected())
			connection();
		;
		try {
			PreparedStatement q = connection
					.prepareStatement("SELECT " + section + " FROM " + table + " WHERE " + section + " = ?");
			q.setString(1, search);
			ResultSet resultat = q.executeQuery();
			boolean hasAccount = resultat.next();
			q.close();
			return hasAccount;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void create(String table, Section[] sections) {
		if (!isConnected())
			connection();
		try {
			String section = "";
			String q = "";
			for (Section s : sections) {
				if (section == "") {
					section += s.getKey();
				} else {
					section += "," + s.getKey();
				}
				if (q == "") {
					q += "?";
				} else {
					q += ",?";
				}
			}
			PreparedStatement qs = connection
					.prepareStatement("INSERT INTO " + table + "(" + section + ") VALUES (" + q + ")");
			int position = 1;
			String values = "";
			for (Section s : sections) {
				if (s.getValue() != null) {
					if (values == "") {
						values += s.getValue();
					} else {
						values += "," + s.getValue();
					}
					if (s.getValueType().equals(Section.Type.STRING)) {
						qs.setString(position, s.getValue().toString());
					} else if (s.getValueType().equals(Section.Type.INTEGER)) {
						qs.setInt(position, (Integer) s.getValue());
					} else if (s.getValueType().equals(Section.Type.BOOLEAN)) {
						qs.setBoolean(position, (Boolean) s.getValue());
					}
				}
				position++;
			}
			qs.execute();
			qs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void remove(String table, String key, String value) {
		if (!isConnected())
			connection();
		;
		try {
			PreparedStatement rs = connection
					.prepareStatement("DELETE FROM " + table + " WHERE " + key + " = '" + value + "'");
			rs.execute();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//TOUTES LES METHODES CI-DESSOUS SONT DES METHODES EXTENDU DE LA CLASSE 'RequestDatabase'

	@Override
	public boolean hasUser(String name) {
		return has(Table.USER.getTable(), "name", name);
	}

	@Override
	public boolean hasData(UUID uuid) {
		return has(Table.UUID.getTable(), "uuid", uuid.toString());
	}

	@Override
	public String getUUID(String name) {
		if (!(hasUser(name)))
			return null;
		return getString(Table.USER.getTable(), "name", name, "uuid");
	}

	@Override
	public BloostryPlayer getData(UUID uuid) {
		if (!(hasData(uuid)))
			return null;
		return Main.bloostryAPI.getGsonInstance().fromJson(getString(Table.UUID.getTable(), "uuid", uuid.toString(), "data"), BloostryPlayer.class);
	}
	
	@Override
	public void createUser(String name, UUID uuid) {
		if(!(hasUser(user))) {
			create(Table.USER.getTable(), new Section[] {new Section("name", Section.Type.STRING, name),new Section("uuid", Section.Type.STRING, uuid.toString())});
		} else {
			Console.sendMessageDebug(PREFIX + "�cThis user is already created ("+name+","+uuid.toString()+")");
		}
	}
	
	@Override
	public void createData(UUID uuid, BloostryPlayer bloostryPlayer) {
		String bloostryPlayerJson = Main.bloostryAPI.getGsonInstance().toJson(bloostryPlayer);
		if(!(hasData(uuid))) {
			create(Table.UUID.getTable(), new Section[] {new Section("uuid", Section.Type.STRING, uuid.toString()),new Section("data", Section.Type.STRING, bloostryPlayerJson)});
		} else {
			Console.sendMessageDebug(PREFIX + "�cThis data is already created ("+ uuid.toString() + "," + bloostryPlayerJson + ")");
		}
	}
	
	@Override
	public void deleteUser(String name) {
		if(hasUser(name)) {
			remove(Table.USER.getTable(), "name", name);
		} else {
			Console.sendMessageDebug(PREFIX + "�cThis user is not created ("+ name + ")");
		}
	}
	
	@Override
	public void deleteData(UUID uuid) {
		if(hasData(uuid)) {
			remove(Table.UUID.getTable(), "uuid", uuid.toString());
		} else  {
			Console.sendMessageDebug(PREFIX + "�cThis data is not created ("+ uuid.toString() + ")");
		}
	}

	@Override
	public void setData(UUID uuid, BloostryPlayer bloostryPlayer) {
		String bloostryPlayerJson = Main.bloostryAPI.getGsonInstance().toJson(bloostryPlayer);
		if(hasData(uuid)) {
			setString(Table.UUID.getTable(), "uuid", uuid.toString(), "data", bloostryPlayerJson);	
		} else {
			Console.sendMessageDebug(PREFIX + "�cThis data is not created ("+ uuid + ")");
		}
	}
}

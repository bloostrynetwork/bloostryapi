package mc.bloostrynetwork.antox11200.bloostryapi.utils.database.mysql;

public enum Table {

	UUID("uuid"),
	USER("user");
	
	private String table;
	
	Table(String table){
		this.table = table;
	}
	
	public String getTable() {
		return this.table;
	}
}

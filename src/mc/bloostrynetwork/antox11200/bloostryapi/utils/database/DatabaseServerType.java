package mc.bloostrynetwork.antox11200.bloostryapi.utils.database;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;

public enum DatabaseServerType {

	REDIS(null), 
	MYSQL(null);

	private RequestDatabase requestDatabase;

	DatabaseServerType(RequestDatabase requestDatabase) {
		this.requestDatabase = requestDatabase;
	}

	public RequestDatabase getRequestDatabase() {
		return requestDatabase;
	}

	public void setRequestDatabase(RequestDatabase requestDatabase) {
		this.requestDatabase = requestDatabase;
	}
	
	public static void init() {
		REDIS.setRequestDatabase(Main.bloostryAPI.getDatabaseManager().getRedisClient());
		MYSQL.setRequestDatabase(Main.bloostryAPI.getDatabaseManager().getMySQLClient());
		Console.sendMessageInfo("�6The DatabaseServerType is initialized for variable :D");
	}
}

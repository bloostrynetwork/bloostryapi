package mc.bloostrynetwork.antox11200.bloostryapi.utils.database;

import java.util.UUID;

import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;

public abstract class RequestDatabase {

	public abstract boolean hasUser(String name);
	public abstract boolean hasData(UUID uuid);

	public abstract void createUser(String name, UUID uuid);
	public abstract void createData(UUID uuid, BloostryPlayer Data);

	public abstract void deleteUser(String name);
	public abstract void deleteData(UUID uuid);
	
	public abstract void setData(UUID uuid, BloostryPlayer bloostryPlayer);
	
	public abstract String getUUID(String name);
	
	public abstract BloostryPlayer getData(UUID uuid);
	
	public boolean hasData(String name) {
		UUID uuid = UUID.fromString(getUUID(name));
		return hasData(uuid);
	}
	
	public BloostryPlayer getData(String name) {
		UUID uuid = UUID.fromString(getUUID(name));
		return getData(uuid);
	}
}

package mc.bloostrynetwork.antox11200.bloostryapi.utils.database.redis;

import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import com.google.gson.Gson;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.IntegerUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.RequestDatabase;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStatus;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisClient extends RequestDatabase {

	public final String PATH_BLOOSTRYPLAYER = "bloostryPlayers:";
	public final String PATH_SERVER_STATUS = "serverStatus:status:";
	public final String PATH_SERVER_ONLINE = "serverStatus:playersonline:";

	public static final String PREFIX = "�8[�cRedis�8] ";

	private String name;
	private String adress;
	private String password;

	private int port;

	public JedisPool jedisPool;

	private BukkitTask bukkitTask;

	private int tryConnectionSuccesNumber;
	private int tryConnectionFailedNumber;

	public RedisClient(String name, String adress, int port, String password) {
		this.name = name;
		this.adress = adress;
		this.port = port;
		this.password = password;
	}

	public void initializedConnection() {
		connectionJedis();

		this.bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously(Main.javaPlugin, new Runnable() {
			@Override
			public void run() {
				Console.sendMessageDebug("�bTry close connection on Jedis !");
				try {
					tryConnectionSuccesNumber++;
					Jedis jedis = jedisPool.getResource();
					long currentTime = System.currentTimeMillis();
					jedis.ping();
					long ping = System.currentTimeMillis() - currentTime;
					jedisPool.getResource().close();
					Console.sendMessageDebug("�aThe closing connecting is succes. �7(�6PING: �e"+ping+"�7) �7[�6Try Connection Succes: �a"+tryConnectionSuccesNumber+"�8, �6Try Connection Failed: �c"+tryConnectionFailedNumber+"�7]");
				} catch (Exception e) {
					tryConnectionFailedNumber++;
					e.printStackTrace();
					Console.sendMessageDebug("�cError redis connection, Try to reconnect !");
					connectionJedis();
				}
			}
		}, 0, 20*10);

	}

	public void cancelTask() {
		this.bukkitTask.cancel();
	}

	public void connectionJedis() {
		ClassLoader previous = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(RedisClient.class.getClassLoader());
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxTotal(-1);
		config.setJmxEnabled(false);
		this.jedisPool = new JedisPool(config, this.adress, this.port, 0, this.password);
		this.jedisPool.getResource().close();
		Thread.currentThread().setContextClassLoader(previous);
		Console.sendMessageInfo(PREFIX + "�aRedis client is connected from server redis :D");
	}

	public void deconnection() {
		this.jedisPool.destroy();
		Console.sendMessageInfo(PREFIX + "�aRedis client is disconnected from server redis \nGood Bye :D");
	}

	public boolean isConnected() {
		if (jedisPool == null)
			return false;
		if (jedisPool.isClosed())
			return false;
		return true;
	}

	/*
	 * public JedisCluster getJedisCluster() { return this.jedisCluster; }
	 */

	public JedisPool getJedisPool() {
		return this.jedisPool;
	}

	public String getName() {
		return name;
	}

	public String getAdress() {
		return adress;
	}

	public String getPassword() {
		return password;
	}

	public int getPort() {
		return port;
	}

	// TOUTES LES METHODES CI-DESSOUS SONT DES METHODES EXTENDU DE LA CLASSE
	// 'RequestDatabase'

	@Override
	public boolean hasUser(String name) {
		return Main.bloostryAPI.getDatabaseManager().getMySQLClient().hasUser(name);
	}

	@Override
	public boolean hasData(UUID uuid) {
		Jedis jedis = jedisPool.getResource();

			boolean hasData = false;
			if (jedis != null) {
				hasData = jedis.get(PATH_BLOOSTRYPLAYER + uuid.toString()) != null;
				jedis.close();
		}
		return hasData;
	}

	@Override
	public String getUUID(String name) {
		return Main.bloostryAPI.getDatabaseManager().getMySQLClient().getUUID(name);
	}

	@Override
	public BloostryPlayer getData(UUID uuid) {
		if (!(hasData(uuid)))
			return null;
		Jedis jedis = jedisPool.getResource();
		String bloostryPlayerJson = null;
		if (jedis != null) {
			bloostryPlayerJson = jedis.get(PATH_BLOOSTRYPLAYER + uuid.toString());
			jedis.close();
		} else 
			return null;
		
		String firstLetter = bloostryPlayerJson.substring(0, 1);
		String lastLetter = bloostryPlayerJson.substring(bloostryPlayerJson.length() - 1, bloostryPlayerJson.length());
		
		if (firstLetter.equalsIgnoreCase("'") && lastLetter.equalsIgnoreCase("'")) {
			bloostryPlayerJson = bloostryPlayerJson.substring(1, bloostryPlayerJson.length() - 1);
		} else if (firstLetter.equalsIgnoreCase("'") && !lastLetter.equalsIgnoreCase("'")) {
			bloostryPlayerJson = bloostryPlayerJson.substring(1, bloostryPlayerJson.length());
		} else if (!firstLetter.equalsIgnoreCase("'") && lastLetter.equalsIgnoreCase("'")) {
			bloostryPlayerJson = bloostryPlayerJson.substring(0, bloostryPlayerJson.length() - 1);
		}
		
		bloostryPlayerJson = bloostryPlayerJson.replace("\\", "");
		
		Console.sendMessageDebug(PREFIX + bloostryPlayerJson);
		
		return Main.bloostryAPI.getGsonInstance().fromJson(bloostryPlayerJson, BloostryPlayer.class);
	}

	@Override
	public void createUser(String name, UUID uuid) {
		Main.bloostryAPI.getDatabaseManager().getMySQLClient().createUser(name, uuid);
	}

	@Override
	public void createData(UUID uuid, BloostryPlayer bloostryPlayer) {
		String bloostryPlayerJson = Main.bloostryAPI.getGsonInstance().toJson(bloostryPlayer);
		if (!(hasData(uuid))) {
			setData(uuid, bloostryPlayer);
		} else {
			Console.sendMessageDebug(PREFIX + "�cThis data is not created (" + uuid + "," + bloostryPlayerJson + ")");
		}
	}

	@Override
	public void deleteUser(String name) {
		Main.bloostryAPI.getDatabaseManager().getMySQLClient().deleteUser(name);
	}

	@Override
	public void deleteData(UUID uuid) {
		if (hasData(uuid)) {
			Jedis jedis = jedisPool.getResource();

			if (jedis != null) {
				jedis.del(PATH_BLOOSTRYPLAYER + uuid.toString());
				jedis.close();
			}

		} else {
			Console.sendMessageDebug(PREFIX + "�cThis data is not created (" + uuid + ")");
		}
	}

	@Override
	public void setData(UUID uuid, BloostryPlayer bloostryPlayer) {
		Jedis jedis = jedisPool.getResource();

		String bloostryPlayerJson = Main.bloostryAPI.getGsonInstance().toJson(bloostryPlayer);
		if (jedis != null) {
			jedis.set(PATH_BLOOSTRYPLAYER + uuid.toString(), "'"+bloostryPlayerJson+"'");
			jedis.close();
			Console.sendMessageDebug("SET " + PATH_BLOOSTRYPLAYER + uuid.toString() + " '" + bloostryPlayerJson + "' lenght: "+bloostryPlayerJson.length());
			
			bloostryPlayer = getData(uuid);
			
			Console.sendMessageDebug("�9Testing recovered DATA -> BloostryPlayer -> "+bloostryPlayer);
		}
	}

	public void addPlayerOnline(String serverName) {
		Jedis jedis = jedisPool.getResource();

		Integer playersOnline = getPlayersOnline(serverName);
		if (jedis != null) {
			jedis.set(PATH_SERVER_ONLINE + serverName, ((playersOnline == null ? 0 : playersOnline) + 1) + "");
			jedis.close();
		}
	}

	public void removePlayerOnline(String serverName) {
		Jedis jedis = jedisPool.getResource();

		Integer playersOnline = getPlayersOnline(serverName);
		if (jedis != null) {
			jedis.set(PATH_SERVER_ONLINE + serverName, ((playersOnline == null ? 0 : playersOnline) - 1) + "");
			jedis.close();
		}
	}

	public Integer getPlayersOnline(String serverName) {
		Jedis jedis = jedisPool.getResource();

		String reponse = jedis.get(PATH_SERVER_ONLINE + serverName);
		if (jedis != null) {
			jedis.close();
		}
		if (reponse == null)
			return null;
		if (!IntegerUtils.isInteger(reponse))
			return 0;
		return IntegerUtils.getValueOf(reponse);
	}

	public Integer getPlayersOnlineOnNetwork() {
		Jedis jedis = jedisPool.getResource();

		Set<String> servers = jedis.smembers("serversList");
		if (jedis != null) {
			jedis.close();
		}
		Integer onlines = 0;
		for (String serverName : servers) {
			onlines += (getPlayersOnline(serverName) == null ? 0 : getPlayersOnline(serverName));
		}
		return onlines;
	}

	public void setServerStatus(String serverName, ServerStatus serverStatus) {
		Jedis jedis = jedisPool.getResource();

		if (jedis != null) {
			jedis.set(PATH_SERVER_STATUS + serverName, serverStatus.getKeyPath());
			jedis.close();
		}
	}

	public ServerStatus getServerStatus(String serverName) {
		Jedis jedis = jedisPool.getResource();

		String status = jedis.get(PATH_SERVER_STATUS + serverName);
		if (jedis != null) {
			jedis.close();
		}
		if (status == null)
			return null;
		return ServerStatus.getServerStatusType(status);
	}

	public Set<String> getServersList() {
		Jedis jedis = jedisPool.getResource();
		Set<String> servers = jedis.smembers("serversList");
		if (jedis != null) {
			jedis.close();
		}
		return servers;
	}

	public void closeServer() {
		setServerStatus(BloostryAPI.getServerName(), ServerStatus.CLOSED);
		Jedis jedis = jedisPool.getResource();
		if (jedis != null) {
			jedis.set(
					Main.bloostryAPI.getDatabaseManager().getRedisClient().PATH_SERVER_ONLINE + BloostryAPI.getServerName(),
					0 + "");
			jedis.close();
		}
	}
}
package mc.bloostrynetwork.antox11200.bloostryapi.utils.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public interface ICommand {
	
	public abstract void handle(CommandSender commandSender, String[] args);
	public abstract String getCommand();
	public abstract TabCompleter getTabCompleter();
	
}

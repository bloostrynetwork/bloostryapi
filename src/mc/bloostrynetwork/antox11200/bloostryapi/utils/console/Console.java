package mc.bloostrynetwork.antox11200.bloostryapi.utils.console;

import org.bukkit.Server;

public class Console {

	private static Server server;
	public static boolean debug;
	
	public Console(Server newServer, boolean debugConsole){
		server = newServer;
		debug = debugConsole;
	}
	
	public static void sendMessageWarning(String message){
		server.getConsoleSender().sendMessage("�f[�eWarning�f] �f: �e"+ message);
	}
	
	public static void sendMessageError(String message){
		server.getConsoleSender().sendMessage("�f[�cError�f] �f: �c"+ message);
	}
	
	public static void sendMessageInfo(String message){
		server.getConsoleSender().sendMessage("�f[�aInfo�f] �f: "+ message);
	}
	
	public static void sendMessageDebug(String message){
		if(!debug)return;
		server.getConsoleSender().sendMessage("�f[�bDebug�f] �f: �b"+ message);
	}
}

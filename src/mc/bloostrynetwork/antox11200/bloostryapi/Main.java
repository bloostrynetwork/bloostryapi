package mc.bloostrynetwork.antox11200.bloostryapi;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public static BloostryAPI bloostryAPI;
	public static JavaPlugin javaPlugin;
	
	@Override
	public void onEnable() {
		javaPlugin = this;
		bloostryAPI = new BloostryAPI(this);
		bloostryAPI.onEnable();
		
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		bloostryAPI.onDisable();
		super.onDisable();
	}
}

package mc.bloostrynetwork.antox11200.bloostryapi.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class consoleDebugICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (commandSender instanceof Player) {
			BloostryPlayer bloostryPlayer = BloostryAPI.getBloostryPlayer(((Player) commandSender).getUniqueId());
			if (bloostryPlayer != null) {
				if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.DEVELOPER.getBloostryRank()
						.getPermissionPower()) {
					commandSender.sendMessage(
							BloostryAPI.PREFIX + "�CVous n'avez pas le rang requis pour faire cette commande !");
					return;
				}
			} else {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�CVous n'�tes pas dans la base de donn�e !");
				return;
			}
		}
		if (args.length == 2) {
			if(args[0].equalsIgnoreCase("debug")) {
				if (args[1].equalsIgnoreCase("get")) {
					commandSender.sendMessage("�6Les messages d�bug sont: "+(Console.debug ? "�aactiv�" : "�cd�sactiv�")+"�6.");
				} else if (args[1].equalsIgnoreCase("set")) {
					Console.debug = (!(	Console.debug));
					commandSender.sendMessage("�6Les messages d�bug sont d�sormais "+(Console.debug ? "�aactiv�" : "�cd�sactiv�")+"�6.");
				}	
			} else {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /consoledebug <debug> <get/set>");
			}
		} else {
			commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /consoledebug <debug> <get/set>");
		}
	}

	@Override
	public String getCommand() {
		return "console";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}
}

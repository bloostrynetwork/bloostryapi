package mc.bloostrynetwork.antox11200.bloostryapi.commands.info;

import java.util.Set;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStats;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStatus;
import oshi.util.FormatUtil;

public class serverinfoICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("all")) {
				Set<String> servers = Main.bloostryAPI.getDatabaseManager().getRedisClient().getServersList();
				commandSender.sendMessage("�7�m------------------------------------------");
				commandSender.sendMessage("");
				for(String serverName : servers) {
					ServerStatus serverStatus = Main.bloostryAPI.getDatabaseManager().getRedisClient().getServerStatus(serverName);
					Integer playersOnline = Main.bloostryAPI.getDatabaseManager().getRedisClient().getPlayersOnline(serverName);
					commandSender.sendMessage("�e- "+serverName);
					commandSender.sendMessage("    �7Etat: �f"+(serverStatus == ServerStatus.OPEN ? "�aouvert" : serverStatus == ServerStatus.CLOSED ? "�Cferm�" : "ir�cup�rable"));
					commandSender.sendMessage("    �7Joueur(s) en ligne: �e"+(playersOnline == null ? "0" : playersOnline) + " �fjoueur(s)");
					commandSender.sendMessage("");
				}
				commandSender.sendMessage("");
				commandSender.sendMessage("�7�m------------------------------------------");
			} else {
				String serverName = args[0];
				commandSender.sendMessage("�7�m------------------------------------------");
				commandSender.sendMessage("");
					ServerStatus serverStatus = Main.bloostryAPI.getDatabaseManager().getRedisClient().getServerStatus(serverName);
					Integer playersOnline = Main.bloostryAPI.getDatabaseManager().getRedisClient().getPlayersOnline(serverName);
					commandSender.sendMessage("�e"+serverName);
					commandSender.sendMessage("  �7Etat: �f"+(serverStatus == ServerStatus.OPEN ? "�aouvert" : serverStatus == ServerStatus.CLOSED ? "�Cferm�" : "ir�cup�rable"));
					commandSender.sendMessage("  �7Joueur(s) en ligne: �e"+(playersOnline == null ? "0" : playersOnline) + " �fjoueur(s)");
				commandSender.sendMessage("");
				commandSender.sendMessage("�7�m------------------------------------------");
			}
		} else {
			/*
			 * if(commandSender instanceof Player) { IPlayer iplayer =
			 * BPlayer.getBPlayer(commandSender.getName()); if(iplayer == null) {
			 * commandSender.
			 * sendMessage("�cVotre compte n'est pas acc�ssible dans la base de donn�e !");
			 * return; } if(iplayer.getRank().getLevelPermission() >= new
			 * Responsable().getLevelPermission()) { sendInfo(commandSender); } else {
			 * iplayer.
			 * sendMessage("�cVous n'avez pas la permission d'�x�cuter cette commande !"); }
			 * } else { }
			 */
			sendInfo(commandSender);
		}
	}

	private void sendInfo(CommandSender cmdSender) {
		cmdSender.sendMessage("�7�m--------------------------------------");
		cmdSender.sendMessage("");
		cmdSender.sendMessage("�6Information du serveur:");
		cmdSender.sendMessage("");
		cmdSender.sendMessage("  �eTicks par seconde: �8( �e" + ServerStats.getTPS() + "�6/s �7/ �e20.0/s �8)");
		cmdSender.sendMessage("");
		cmdSender.sendMessage("  �eM�moire:");
		cmdSender.sendMessage("    �7disponible: �f"
				+ (ServerStats.getMaximumMemoryRAM() - ServerStats.getUsedMemoryRAM()) + " �6MB");
		cmdSender.sendMessage("    �7utilis�: �f" + ServerStats.getUsedMemoryRAM() + " �6MB");
		cmdSender.sendMessage("    �7total: �f" + ServerStats.getMaximumMemoryRAM() + " �6MB");
		cmdSender.sendMessage("");
		cmdSender.sendMessage("  �eProcesseur �8(�cBeta�8)�e:");

		try {
		cmdSender.sendMessage("    �7lecture: �f" + FormatUtil.formatBytes(ServerStats.getProcessorRead()));
		cmdSender.sendMessage("    �7�criture: �f" + FormatUtil.formatBytes(ServerStats.getProcessorWritten()));
		} catch  (Exception e) {
			cmdSender.sendMessage("�c  Les informations n'ont pas pu �tre r�cup�rer");
		}
		cmdSender.sendMessage("");
		cmdSender.sendMessage("�7�m--------------------------------------");
	}

	@Override
	public String getCommand() {
		return "serverinfo";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

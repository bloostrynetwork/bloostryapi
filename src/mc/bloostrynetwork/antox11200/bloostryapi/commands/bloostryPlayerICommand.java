package mc.bloostrynetwork.antox11200.bloostryapi.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.IntegerUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class bloostryPlayerICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length >= 2) {
			BloostryPlayer bloostryPlayer;
			if (commandSender instanceof Player) {
				Player player = (Player) commandSender;
				bloostryPlayer = BloostryAPI.getBloostryPlayer(player.getName());
				if (bloostryPlayer == null) {
					commandSender.sendMessage(BloostryAPI.PREFIX + "�cVous n'�tes pas dans la base de donn�e !");
					return;
				}
			}
			String target = args[0];
			BloostryPlayer bloostryPlayerTarget = BloostryAPI.getBloostryPlayer(target);
			if (bloostryPlayerTarget == null) {
				commandSender
						.sendMessage(BloostryAPI.PREFIX + "�cCe joueur n'est pas contenu dans la base de donn�e !");
				return;
			}
			if (args[1].equalsIgnoreCase("get")) {
				commandSender.sendMessage("�7�m-------------------------------------------");
				commandSender.sendMessage("");
				String centerNamed = "                                    ";
				int pos = centerNamed.length() / target.length();
				String result = "";
				for (int i = 0; i < pos; i++) {
					result += " ";
				}
				commandSender.sendMessage(result + "�e" + target + "�7 - "
						+ (bloostryPlayerTarget.isOnline() ? "�aEn ligne" : "�CHost ligne"));
				commandSender.sendMessage("");
				commandSender.sendMessage("�7Rang: " + bloostryPlayerTarget.getBloostryRank().getPrefix());
				commandSender.sendMessage("�7BloostryCoins: �f" + bloostryPlayerTarget.getBloostryCoins());
				commandSender.sendMessage("�7MystiCoins: �f" + bloostryPlayerTarget.getMystiCoins());
				commandSender.sendMessage("");
				commandSender.sendMessage("�7�m-------------------------------------------");
			} else if (args[1].equalsIgnoreCase("bloostryCoins")) {
				if (commandSender instanceof Player) {
					Player player = (Player) commandSender;
					bloostryPlayer = BloostryAPI.getBloostryPlayer(player.getName());
					if (bloostryPlayer == null) {
						commandSender.sendMessage(BloostryAPI.PREFIX + "�cVous n'�tes pas dans la base de donn�e !");
						return;
					} else {
						if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank()
								.getPermissionPower()) {
							commandSender.sendMessage(
									BloostryAPI.PREFIX + "�cVous n'avez pas le rang requis pour faire cette commande");
							return;
						}
					}
				}
				if (args.length == 4) {
					if (args[2].equalsIgnoreCase("add")) {
						if (IntegerUtils.isInteger(args[3])) {
							int amount = IntegerUtils.getValueOf(args[3]);
							bloostryPlayerTarget.addBloostryCoins(amount);
							commandSender.sendMessage(
									"�6Vous venez d'ajouter �a" + amount + " �eBloostryCoins �6pour " + target);
						} else {
							commandSender.sendMessage(BloostryAPI.PREFIX
									+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
						}
					} else if (args[2].equalsIgnoreCase("remove")) {
						if (IntegerUtils.isInteger(args[3])) {
							int amount = IntegerUtils.getValueOf(args[3]);
							bloostryPlayerTarget.removeBloostryCoins(amount);
							commandSender.sendMessage(
									"�6Vous venez retirer �c" + amount + " �eBloostryCoins �6pour " + target);
						} else {
							commandSender.sendMessage(BloostryAPI.PREFIX
									+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
						}
					} else {
						commandSender.sendMessage(BloostryAPI.PREFIX
								+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
					}
				} else {
					commandSender.sendMessage(BloostryAPI.PREFIX
							+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
				}
			} else if (args[1].equalsIgnoreCase("mystiCoins")) {
				if (commandSender instanceof Player) {
					Player player = (Player) commandSender;
					bloostryPlayer = BloostryAPI.getBloostryPlayer(player.getName());
					if (bloostryPlayer == null) {
						commandSender.sendMessage(BloostryAPI.PREFIX + "�cVous n'�tes pas dans la base de donn�e !");
						return;
					} else {
						if (bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank()
								.getPermissionPower()) {
							commandSender.sendMessage(
									BloostryAPI.PREFIX + "�cVous n'avez pas le rang requis pour faire cette commande");
							return;
						}
					}
				}
				if (args.length == 4) {
					if (args[2].equalsIgnoreCase("add")) {
						if (IntegerUtils.isInteger(args[3])) {
							int amount = IntegerUtils.getValueOf(args[3]);
							bloostryPlayerTarget.addMystiCoins(amount);
							commandSender.sendMessage(
									"�6Vous venez d'ajouter �a" + amount + " �eMystiCoins �6pour " + target);
						} else {
							commandSender.sendMessage(BloostryAPI.PREFIX
									+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
						}
					} else if (args[2].equalsIgnoreCase("remove")) {
						if (IntegerUtils.isInteger(args[3])) {
							int amount = IntegerUtils.getValueOf(args[3]);
							bloostryPlayerTarget.removeMystiCoins(amount);
							commandSender
									.sendMessage("�6Vous venez retirer �c" + amount + " �eMystiCoins �6pour " + target);
						} else {
							commandSender.sendMessage(BloostryAPI.PREFIX
									+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
						}
					} else {
						commandSender.sendMessage(BloostryAPI.PREFIX
								+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
					}
				} else {
					commandSender.sendMessage(BloostryAPI.PREFIX
							+ "Erreur, essayez: /bloostryplayer <joueur> bloostryCoins <add/remove> <nombre>");
				}
			} else {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /bloostryplayer <joueur> <get>");
				commandSender.sendMessage(BloostryAPI.PREFIX
						+ "                   /bloostryplayer <joueur> <bloostryCoins/mystiCoins> <add/remove> <nombre>");
			}
		} else {
			commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /bloostryplayer <joueur> <get/bloostrycoins/mysticoins>");
		}
	}

	@Override
	public String getCommand() {
		return "bloostryplayer";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return new TabCompleter() {
			@Override
			public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
				if (args.length == 2) {
					ArrayList<String> list = new ArrayList<>();
					list.add("get");
					list.add("bloostrycoins");
					list.add("mysticoins");
					return list;
				} else if (args.length == 3) {
					if (args[1].equalsIgnoreCase("bloostryCoins") || args[1].equalsIgnoreCase("bloostryCoins")) {
						ArrayList<String> list = new ArrayList<>();
						list.add("add");
						list.add("remove");
						return list;
					}
					return null;
				}
				return null;
			}
		};
	}
}

package mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.PunishmentType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.TimeUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.network.BungeeCommunication;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class tempmuteICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length >= 4) {
			String target = args[0];
			BloostryPlayer bloostryPlayerTarget = BloostryAPI.getBloostryPlayer(target);
			if (bloostryPlayerTarget == null) {
				commandSender.sendMessage(
						BloostryAPI.PREFIX + "�CCe joueur n'est pas enregistr� dans la base de donn�e !");
				return;
			}

			if (commandSender instanceof Player) {
				BloostryPlayer iplayerSender = BloostryAPI.getBloostryPlayer(commandSender.getName());
				if(iplayerSender.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous n'avez pas le rang requis pour faire cette commande");
					return;
				}
				if (bloostryPlayerTarget.getBloostryRank().getPermissionPower() > iplayerSender.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(
							BloostryAPI.PREFIX + "�cVous ne pouvez pas mute les joueur plus haut grad� que vous !");
					return;
				}
				if (bloostryPlayerTarget.getBloostryRank().getPermissionPower() == iplayerSender.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous ne pouvez pas mute les joueur qui on le m�me grade que vous !");
					return;
				}
			}

			if (bloostryPlayerTarget.isMuted() != null) {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�cCe joueur est d�j� mute !");
				return;
			}

			int time = 0;

			try {
				Integer integerTime = Integer.valueOf(args[1]);
				time = integerTime;
			} catch (Exception e) {
				commandSender.sendMessage(BloostryAPI.PREFIX
						+ "�cErreur, essayez: /tempmute <joueur> <temps> <s/m/h/d/w/month> <raison>");
				return;
			}

			Long ticks = TimeUtils.getTicks(args[2], time);

			if (ticks == null) {
				commandSender.sendMessage(BloostryAPI.PREFIX
						+ "�cErreur, essayez: /tempmute <joueur> <temps> <s/m/h/d/w/month> <raison>");
				return;
			}

			ticks += System.currentTimeMillis();

			String reason = "";
			for (int i = 3; i < args.length; i++) {
				if (reason == "") {
					reason = args[i];
				} else {
					reason += " " + args[i];
				}
			}

			bloostryPlayerTarget.addSanction(SanctionType.TEMPORARY,
					new Sanction(PunishmentType.MUTE, ticks, commandSender.getName(), reason));

			BungeeCommunication.sendBroadcast(BloostryAPI.PREFIX + "�c" + target + " �7vient d'�tre mute temporairement par �c"
					+ commandSender.getName() + " �7pour �7'�c" + reason + "�7'");

			bloostryPlayerTarget.sendMessage(
					BloostryAPI.PREFIX + "�cVous avez �t� temporairement mute par �f" + commandSender.getName()
							+ " �cpour �7'�f" + reason + "�7' �cpendant �f" + TimeUtils.getMSG(ticks));
		} else {
			commandSender.sendMessage(BloostryAPI.PREFIX
					+ "�cErreur, essayez: /tempmute <joueur> <temps> <s/m/h/d/w/month> <raison>");
		}
	}

	@Override
	public String getCommand() {
		return "tempmute";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

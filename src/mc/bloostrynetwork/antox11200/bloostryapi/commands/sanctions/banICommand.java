package mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.PunishmentType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.network.BungeeCommunication;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class banICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		// Exemple: /ban Prosternity <raison>

		if (args.length >= 2) {
			String target = args[0];
			BloostryPlayer bloostryPlayerTarget = BloostryAPI.getBloostryPlayer(target);
			if (bloostryPlayerTarget == null) {
				commandSender.sendMessage(
						BloostryAPI.PREFIX + "�CCe joueur n'est pas enregistr� dans la base de donn�e !");
				return;
			}

			if (commandSender instanceof Player) {
				BloostryPlayer bloostryPlayerSender = BloostryAPI.getBloostryPlayer(commandSender.getName());
				if(bloostryPlayerSender.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank().getPermissionPower()) {
					bloostryPlayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous n'avez pas le rang requis pour faire cette commande");
					return;
				}
				if (bloostryPlayerTarget.getBloostryRank().getPermissionPower() > bloostryPlayerSender.getBloostryRank().getPermissionPower()) {
					bloostryPlayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous ne pouvez pas bannir les joueur plus haut grad� que vous !");
					return;
				}
				if (bloostryPlayerTarget.getBloostryRank().getPermissionPower() == bloostryPlayerSender.getBloostryRank().getPermissionPower()) {
					bloostryPlayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous ne pouvez pas bannir les joueur qui on le m�me grade que vous !");
					return;
				}
			}

			if (bloostryPlayerTarget.isBanned() != null) {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�cCe joueur est d�j� banni");
				return;
			}

			String reason = "";
			for (int i = 1; i < args.length; i++) {
				if (reason == "") {
					reason = args[i];
				} else {
					reason += " " + args[i];
				}
			}

			bloostryPlayerTarget.addSanction(SanctionType.PERMANENT,
					new Sanction(PunishmentType.BAN, -1, commandSender.getName(), reason));
			
			BungeeCommunication.sendBroadcast(BloostryAPI.PREFIX + "�c" + target + " �7vient d'�tre banni par �c"
					+ commandSender.getName() + " �7pour �7'�c" + reason + "�7'");
			
			BungeeCommunication.kickPlayer(bloostryPlayerTarget.getName(), "�cVous �tes banni de ce serveur \n�cAuteur: �f" + commandSender.getName()
					+ " \n�cRaison: �f" + reason + " \n�cPendant: �fPermanent"
					+ "\n�6 \n�7Si vous voulez contester votre bannisement veuillez vous rendre sur notre teamspeak \n�ats.bloostrynetwork.fr");
		} else {
			commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /ban <joueur> <raison>");
		}
	}

	@Override
	public String getCommand() {
		return "ban";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

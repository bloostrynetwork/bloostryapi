package mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.PunishmentType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.network.BungeeCommunication;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class muteICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length >= 2) {
			String target = args[0];
			BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(target);
			if (iplayer == null) {
				commandSender.sendMessage(
						BloostryAPI.PREFIX + "�CCe joueur n'est pas enregistr� dans la base de donn�e !");
				return;
			}
			
			if (commandSender instanceof Player) {
				BloostryPlayer iplayerSender = BloostryAPI.getBloostryPlayer(commandSender.getName());
				if(iplayerSender.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous n'avez pas le rang requis pour faire cette commande");
					return;
				}
				if (iplayer.getBloostryRank().getPermissionPower() > iplayerSender.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous ne pouvez pas mute les joueur plus haut grad� que vous !");
					return;
				}
				if (iplayer.getBloostryRank().getPermissionPower() == iplayerSender.getBloostryRank().getPermissionPower()) {
					iplayerSender.sendMessage(BloostryAPI.PREFIX
							+ "�cVous ne pouvez pas mute les joueur qui on le m�me grade que vous !");
					return;
				}
			}

			if(iplayer.isMuted() != null) {
				commandSender.sendMessage(BloostryAPI.PREFIX+ "�cCe joueur est d�j� mute");
				return;
			}

			String reason = "";
			for (int i = 1; i < args.length; i++) {
				if (reason == "") {
					reason = args[i];
				} else {
					reason += " " + args[i];
				}
			}

			iplayer.addSanction(SanctionType.PERMANENT,
					new Sanction(PunishmentType.MUTE, -1, commandSender.getName(), reason));
			
			BungeeCommunication.sendBroadcast(BloostryAPI.PREFIX + "�c" + target + " �7vient d'�tre mute par �c"
					+ commandSender.getName() + " �7pour  �7'�c" + reason+"�7'");
			
			iplayer.sendMessage(BloostryAPI.PREFIX +"�cVous avez �t� mute par �f"+commandSender.getName()+" �cpour �7'�f"+reason+"�7'");
		} else {
			commandSender.sendMessage(BloostryAPI.PREFIX + "�cErreur, essayez: /mute <joueur> <raison>");
		}
	}

	@Override
	public String getCommand() {
		return "mute";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

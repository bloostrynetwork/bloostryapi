package mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.PunishmentType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.SanctionType;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.TimeUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.DateUtils;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class historyICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if (args.length == 1) {
			String targetName = args[0];
			BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(targetName);
			if (iplayer == null) {
				commandSender.sendMessage("�CCe joeur n'est pas enregistrer dans la base de donn�e");
				return;
			}
			commandSender.sendMessage("�7�M-----------------------------------------------");
			commandSender.sendMessage("");
			String centerNamed = "                                    ";
			int pos = centerNamed.length() / targetName.length();
			String result = "";
			for (int i = 0; i < pos; i++) {
				result += " ";
			}
			commandSender.sendMessage(
					result + "�e" + targetName + "�7 - " + (iplayer.isOnline() ? "�aEn ligne" : "�CHost ligne"));
			commandSender.sendMessage("");
			commandSender.sendMessage("�6Actuellement banni: " + (iplayer.isBanned() != null ? "�cOui" : "�aNon"));
			commandSender.sendMessage("�6Actuellement mute: " + (iplayer.isMuted() != null ? "�cOui" : "�aNon"));
			commandSender.sendMessage("");

			int ban = 0;
			int mute = 0;
			int kick = 0;

			List<String> sanctionsMessage = new ArrayList<>();
			for (SanctionType sanctionType : SanctionType.values()) {
				if (iplayer.getSanctionList(sanctionType) != null) {
					for (Sanction sanctions : iplayer.getSanctionList(sanctionType)) {
						if (sanctions.getPunishmentType() == PunishmentType.BAN) {
							ban++;
						} else if (sanctions.getPunishmentType() == PunishmentType.MUTE) {
							mute++;
						} else if (sanctions.getPunishmentType() == PunishmentType.KICK) {
							kick++;
						}
						if (sanctions.getPunishmentType() == PunishmentType.KICK) {
							sanctionsMessage.add("�7- �e" + sanctions.getPunishmentType().name.replace("mut�e", "mute")
									+ " �6par �c" + sanctions.getAuthor() + " �6pour �e" + sanctions.getReason());
						} else {
							sanctionsMessage.add("�7- �e" + sanctions.getPunishmentType().name.replace("mut�e", "mute")
									+ " �6par �c" + sanctions.getAuthor() + " �6pour �e" + sanctions.getReason() + " "
									+ (sanctions.getSanctionType() == SanctionType.PERMANENT ? "�6pendant: �ePermanent"
											: (sanctions.getTimeLeft() - System.currentTimeMillis() <= 0
													? "�6pendant: �e" + DateUtils.getTimerDate(sanctions.getFor())
															+ " �6temp restant: �e0 seconde"
													: "�6pendant: �e" + DateUtils.getTimerDate(sanctions.getFor())
															+ " �6temp restant: �e"
															+ TimeUtils.getMSG(sanctions.getTimeLeft()))));
						}
					}
				}
			}
			commandSender.sendMessage("�6Nombre de ban: �e" + ban);
			commandSender.sendMessage("�6Nombre de mute: �e" + mute);
			commandSender.sendMessage("�6Nombre de kick: �e" + kick);
			commandSender.sendMessage("");
			commandSender.sendMessage("�6Liste des sanctions:");
			if (sanctionsMessage.size() <= 0) {
				commandSender.sendMessage("�cAucune sanction trouv�e !");
			} else {
				for (String s : sanctionsMessage) {
					commandSender.sendMessage(s);
				}
			}
			commandSender.sendMessage("");
			commandSender.sendMessage("�7�M-----------------------------------------------");
		} else if (args.length == 2) {
			if (args[0].equalsIgnoreCase("clear")) {
				if (commandSender instanceof Player) {
					BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(commandSender.getName());
					if (iplayer.getBloostryRank().getPermissionPower() < Ranks.DEVELOPER.getBloostryRank()
							.getPermissionPower()) {
						commandSender
								.sendMessage(BloostryAPI.PREFIX + "�cVous n'avez pas le rang requis pour faire ceci !");
						return;
					}
				}
				BloostryPlayer itarget = BloostryAPI.getBloostryPlayer(args[1]);
				if (itarget != null) {
					itarget.getSanctions().clear();
					itarget.refresh();
					commandSender.sendMessage("�aRequette envoy�e avec succ�s :D");
				} else {
					commandSender.sendMessage(
							BloostryAPI.PREFIX + "�cCe joueur n'est pas enregistrer dans la base de donn�e !");
				}
			}
		} else {
			commandSender.sendMessage("�CErreur, essayez: (�c/history <joueur>�f,�c/history clear <joueur>)");
		}
	}

	@Override
	public String getCommand() {
		return "history";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

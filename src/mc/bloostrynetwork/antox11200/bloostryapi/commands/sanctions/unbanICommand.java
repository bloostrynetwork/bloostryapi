package mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.player.sanction.Sanction;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.network.BungeeCommunication;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class unbanICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		if(args.length == 1) {
			String targetName = args[0];
			BloostryPlayer iplayer = BloostryAPI.getBloostryPlayer(targetName);
			if(iplayer != null) {
				if(iplayer.getBloostryRank().getPermissionPower() < Ranks.HOST.getBloostryRank().getPermissionPower()) {
					iplayer.sendMessage(BloostryAPI.PREFIX
							+ "�cVous n'avez pas le rang requis pour faire cette commande");
					return;
				}
				Sanction sanction = iplayer.isBanned();
				if(sanction != null) {
					sanction.setEtat(false);
					iplayer.refresh();
					BungeeCommunication.sendBroadcast(BloostryAPI.PREFIX+"�7"+targetName+" �cvient d'�tre d�banni par �7"+commandSender.getName());
				} else {
					commandSender.sendMessage(BloostryAPI.PREFIX+"�CCe joueur n'est pas banni !");
				}
			} else {
				commandSender.sendMessage(BloostryAPI.PREFIX+"�cCe joueur n'est pas enregistr� dans la base de donn�e !");
			}
		} else  {
			commandSender.sendMessage(BloostryAPI.PREFIX+"�cErreur, essayez: /unban <joueur>");
		}
	}

	@Override
	public String getCommand() {
		return "unban";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

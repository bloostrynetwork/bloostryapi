package mc.bloostrynetwork.antox11200.bloostryapi.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import mc.bloostrynetwork.antox11200.bloostryapi.BloostryAPI;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.BloostryRank;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.rank.Ranks;

public class rankICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		BloostryPlayer bloostryPlayer = null;
		if (commandSender instanceof Player) {
			bloostryPlayer = BloostryAPI.getBloostryPlayer(commandSender.getName());
			if (bloostryPlayer == null) {
				commandSender.sendMessage(
						BloostryAPI.PREFIX + "�cVotre compte n'est pas accesible pour des r�cup�rations de donn�es !");
				return;
			}
		}
		if (args == null || args.length == 0) {
			commandSender.sendMessage("�7�m------------------------------");
			commandSender.sendMessage("");
			commandSender.sendMessage("�6Liste des commandes:");
			commandSender.sendMessage("�e- �f/rank set <joueur> <grade|grade id>");
			commandSender.sendMessage("�e- �f/rank get <joueur>");
			commandSender.sendMessage("");
			commandSender.sendMessage("�7�m------------------------------");
			return;
		}
		/*
		 * Liste des commandes
		 * 
		 * - /rank set <player> <rank> - /rank get <player>
		 * 
		 */
		String cmd2 = args[0];
		if (cmd2.equalsIgnoreCase("list")) {
			commandSender.sendMessage("�7�m------------------------------");
			commandSender.sendMessage("");
			commandSender.sendMessage("�6Liste des rangs:");
			for(Ranks rank : Ranks.values()) {
				BloostryRank bloostryRank = rank.getBloostryRank();
				commandSender.sendMessage("  �e- "+bloostryRank.getNameColered());
			}
			commandSender.sendMessage("");
			commandSender.sendMessage("�7�m------------------------------");
		} else if (cmd2.equalsIgnoreCase("get")) {
			if (args.length == 2) {
				BloostryPlayer iTargetPlayer = BloostryAPI.getBloostryPlayer(args[1]);
				if (iTargetPlayer != null) {
					commandSender.sendMessage(BloostryAPI.PREFIX + "�fLe joueur �7" + iTargetPlayer.getName() + " �f� le grade de "
							+ iTargetPlayer.getBloostryRank().getChatColor() + iTargetPlayer.getBloostryRank().getName()
							+ " �9[�8ID: �7" + iTargetPlayer.getBloostryRank().getId() + "�9]");
					return;
				} else {
					commandSender
							.sendMessage(BloostryAPI.PREFIX + "�cCe joueur n'est pas enregistr� dans notre base de donnn�e !");
				}
			} else {
				commandSender.sendMessage(BloostryAPI.PREFIX + "�cLa commande que vous venez de rentrer est invalide !");
			}
		}

		if ((commandSender instanceof Player) && bloostryPlayer.getBloostryRank().getPermissionPower() < Ranks.RESP_DEVELOPER.getBloostryRank().getPermissionPower()) {
			commandSender.sendMessage(BloostryAPI.PREFIX + "�cVous n'avez pas le bon grade pour �x�ctuter cette commande !");
			return;
		}

		if (args.length > 2) {
			if (cmd2.equalsIgnoreCase("set")) {
				if (args.length == 3) {
					BloostryPlayer iTargetPlayer = BloostryAPI.getBloostryPlayer(args[1]);
					if (iTargetPlayer != null) {
						Ranks rank = null;	
						try {
							Integer id = Integer.valueOf(args[2]);
							rank = Ranks.getBloostryRank(id);
						} catch (Exception e) {
							rank = Ranks.getBloostryRank(args[2]);
						}
						
						if(rank == null) {
							commandSender.sendMessage(BloostryAPI.PREFIX + "�cCe grade n'existe pas !");
							return;	
						}
						
						BloostryRank bloostryRank = rank.getBloostryRank();

						if (bloostryRank != null) {
							if (iTargetPlayer.getBloostryRank().getId() != bloostryRank.getId()) {
								if(bloostryPlayer != null) {
									if(bloostryPlayer.getBloostryRank().getPermissionPower() < bloostryRank.getPermissionPower()) {
										commandSender.sendMessage(BloostryAPI.PREFIX+"�cVous ne pouvez pas d�finir un rang sup�rieur au votre :/");
										return;
									}
								}
								iTargetPlayer.setBloostryRank(bloostryRank);
								commandSender
										.sendMessage(BloostryAPI.PREFIX + "�fVous avez d�fini le grade " + bloostryRank.getChatColor()
												+ bloostryRank.getName() + " �f� �e" + iTargetPlayer.getName() + "�f.");
								iTargetPlayer.sendMessage(
										BloostryAPI.PREFIX + "�e" + commandSender.getName() + " �fvous a d�fini le grade de "
												+ bloostryRank.getChatColor() + bloostryRank.getName() + "�f.");
							} else {
								commandSender.sendMessage(BloostryAPI.PREFIX + "�cCe joueur dispose d�j� de ce grade !");
							}
						} else {
							commandSender.sendMessage(BloostryAPI.PREFIX + "�cCe grade n'existe pas !");
						}
					} else {
						commandSender.sendMessage(
								BloostryAPI.PREFIX + "�cCe joueur n'est pas enregistr� dans notre base de donnn�e !");
					}
				} else {
					commandSender.sendMessage(BloostryAPI.PREFIX + "�cLa commande que vous venez de rentrer est invalide !");
				}
			}
		}
	}

	@Override
	public String getCommand() {
		return "rank";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return new TabCompleter() {
			@Override
			public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
				if (args.length <= 1) {
					ArrayList<String> list = new ArrayList<>();
					list.add("set");
					list.add("get");
					return list;
				}
				return null;
			}
		};
	}
}

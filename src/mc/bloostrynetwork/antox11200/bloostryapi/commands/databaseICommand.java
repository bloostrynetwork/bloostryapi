package mc.bloostrynetwork.antox11200.bloostryapi.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import mc.bloostrynetwork.antox11200.bloostryapi.Main;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.commands.ICommand;

public class databaseICommand implements ICommand {

	@Override
	public void handle(CommandSender commandSender, String[] args) {
		commandSender.sendMessage("�7�m------------------------------------");
		commandSender.sendMessage("");
		commandSender.sendMessage("�6Information des base de donn�es:");
		commandSender.sendMessage("�7-  �eRedis connexion: "+(Main.bloostryAPI.getDatabaseManager().getRedisClient().isConnected() ? "�aactif" : "�cnon actif"));
		commandSender.sendMessage("�7-  �eMySQL connexion: "+(Main.bloostryAPI.getDatabaseManager().getMySQLClient().isConnected() ? "�aactif" : "�cnon actif"));
		commandSender.sendMessage("");
		commandSender.sendMessage("�7�m------------------------------------");
	}

	@Override
	public String getCommand() {
		return "database";
	}

	@Override
	public TabCompleter getTabCompleter() {
		return null;
	}

}

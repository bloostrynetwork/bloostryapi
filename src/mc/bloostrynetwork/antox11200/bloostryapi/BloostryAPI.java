package mc.bloostrynetwork.antox11200.bloostryapi;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mc.bloostrynetwork.antox11200.bloostryapi.commands.bloostryPlayerICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.consoleDebugICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.databaseICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.rankICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.info.serverinfoICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.banICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.historyICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.kickICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.muteICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.tempbanICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.tempmuteICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.unbanICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.commands.sanctions.unmuteICommand;
import mc.bloostrynetwork.antox11200.bloostryapi.manager.CommandsManager;
import mc.bloostrynetwork.antox11200.bloostryapi.manager.DatabaseManager;
import mc.bloostrynetwork.antox11200.bloostryapi.manager.EventsManager;
import mc.bloostrynetwork.antox11200.bloostryapi.runnables.LagRunnable;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.console.Console;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.database.DatabaseServerType;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.player.BloostryPlayer;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStats;
import mc.bloostrynetwork.antox11200.bloostryapi.utils.server.ServerStatus;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class BloostryAPI {

	public static final String PREFIX = "�8[�cInfrastructure�8] ";
	private JavaPlugin javaPlugin;

	public BloostryAPI(JavaPlugin javaPlugin) {
		this.javaPlugin = javaPlugin;
	}

	private Twitter twitter;
	private CommandsManager commandsManager;
	private DatabaseManager databaseManager;
	private Gson gsonInstance;

	public boolean enabled = false;

	public void onLoad() {
	}

	public void onEnable() {
		// Initialisation du syst�me pour les tps
		Bukkit.getScheduler().runTaskTimer(javaPlugin, new LagRunnable(), 100L, 1L);

		// Initialisatino de la classe Console
		new Console(Bukkit.getServer(), false);

		if (!Bukkit.getServer().getIp().equals("62.210.168.176")
				&& !Bukkit.getServer().getIp().equals("play.bloostrynetwork.fr")) {

			for(Player players : Bukkit.getOnlinePlayers()) {
				players.sendMessage("�8�m-----------------------------------------------------");
				players.sendMessage("");
				players.sendMessage("�cErreur fatal, le plugin n'est pas utilisable sur se serveur");
				players.sendMessage("�cveuillez le laisser sur le serveur d'origine !");
				players.sendMessage("");
				players.sendMessage("�7Information:");
				players.sendMessage("  �eAuteur: �fDarkTwister_ (Antox11200)");
				players.sendMessage("  �eVersion: �f" + getJavaPlugin().getDescription().getVersion());
				players.sendMessage("  �eDistribu� pour: �fBloostryNetwork");
				players.sendMessage("");
				players.sendMessage("�cSi le plugin est sur son serveur d'origine veuillez");
				players.sendMessage("�ccontacter le d�veloppeur !");
				players.sendMessage("");
				players.sendMessage("§8§m-----------------------------------------------------");
			}

			Console.sendMessageInfo("�8�m-----------------------------------------------------");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�cErreur fatal, le plugin n'est pas utilisable sur se serveur");
			Console.sendMessageInfo("�cveuillez le laisser sur le serveur d'origine !");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�7Information:");
			Console.sendMessageInfo("  �eAuteur: �fDarkTwister_ (Antox11200)");
			Console.sendMessageInfo("  �eVersion: �f" + getJavaPlugin().getDescription().getVersion());
			Console.sendMessageInfo("  �eDistribu� pour: �fBloostryNetwork");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�cSi le plugin est sur son serveur d'origine veuillez");
			Console.sendMessageInfo("�ccontacter le d�veloppeur !");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�8�m-----------------------------------------------------");

			Bukkit.getPluginManager().disablePlugin(this.javaPlugin);

			this.enabled = false;
		} else {
			this.gsonInstance = new GsonBuilder().create();

			FileConfiguration config = javaPlugin.getConfig();

			config.options().copyDefaults(true);
			javaPlugin.saveConfig();

			// RECUPERATION DES VARIABLE
			String OAuthConsumerKey = config.getString("twitter4j.OAuthConsumerKey");
			String OAuthConsumerSecret = config.getString("twitter4j.OAuthConsumerSecret");
			String OAuthAccessToken = config.getString("twitter4j.OAuthAccessToken");
			String OAuthAccessTokenSecret = config.getString("twitter4j.OAuthAccessTokenSecret");

			String mysqlAdress = config.getString("databases-connector.mysql.adress");
			String mysqlDatabase = config.getString("databases-connector.mysql.database");
			String mysqlUsername = config.getString("databases-connector.mysql.username");
			String mysqlPassword = config.getString("databases-connector.mysql.password");

			String redisAdress = config.getString("databases-connector.redis.adress");
			Integer redisPort = config.getInt("databases-connector.redis.port");
			String redisPassword = config.getString("databases-connector.redis.password");

			ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

			// Connexion au client de twitter (Compte de BloostryNetwork)
			configurationBuilder.setDebugEnabled(true).setOAuthConsumerKey(OAuthConsumerKey)
					.setOAuthConsumerSecret(OAuthConsumerSecret).setOAuthAccessToken(OAuthAccessToken)
					.setOAuthAccessTokenSecret(OAuthAccessTokenSecret);

			TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
			this.twitter = twitterFactory.getInstance();

			Console.sendMessageInfo("�aTwitter account is connected :D");

			// Connexion � tous les base de donn�es

			this.databaseManager = new DatabaseManager();
			this.databaseManager.connectionMySQL(mysqlAdress, mysqlDatabase, mysqlUsername, mysqlPassword); // Connexion
																											// de
																											// MySQL
			this.databaseManager.connectionRedis("bloostryapi", redisAdress, redisPort, redisPassword); // Connexion de
																										// Redis

			// Initialisation de la gestion des commandes
			this.commandsManager = new CommandsManager(this.javaPlugin);

			// Ajouts des commandes
			this.commandsManager.addCommand(javaPlugin, new databaseICommand());
			this.commandsManager.addCommand(javaPlugin, new rankICommand());
			this.commandsManager.addCommand(javaPlugin, new tempbanICommand());
			this.commandsManager.addCommand(javaPlugin, new banICommand());
			this.commandsManager.addCommand(javaPlugin, new muteICommand());
			this.commandsManager.addCommand(javaPlugin, new tempmuteICommand());
			this.commandsManager.addCommand(javaPlugin, new unmuteICommand());
			this.commandsManager.addCommand(javaPlugin, new kickICommand());
			this.commandsManager.addCommand(javaPlugin, new unbanICommand());
			this.commandsManager.addCommand(javaPlugin, new historyICommand());
			this.commandsManager.addCommand(javaPlugin, new serverinfoICommand());
			this.commandsManager.addCommand(javaPlugin, new bloostryPlayerICommand());
			this.commandsManager.addCommand(javaPlugin, new consoleDebugICommand());

			// Enregistrement de toutes les commande
			this.commandsManager.registerCommands();

			new EventsManager().registerListeners();

			DatabaseServerType.init();

			Main.bloostryAPI.getDatabaseManager().getRedisClient().setServerStatus(BloostryAPI.getServerName(),
					ServerStatus.OPEN);

			String serverName = getServerName();

			for (Player players : Bukkit.getOnlinePlayers()) {
				Main.bloostryAPI.getDatabaseManager().getRedisClient().addPlayerOnline(serverName);
			}

			this.enabled = true;

			Console.sendMessageInfo("�7�M----------------------------------------------");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("		�6Bloostry�eAPI");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�eMain: �6" + this.javaPlugin.getDescription().getMain());
			Console.sendMessageInfo("�eAuthor: �6" + this.javaPlugin.getDescription().getName());
			Console.sendMessageInfo("�eVersion: �6" + this.javaPlugin.getDescription().getVersion());
			Console.sendMessageInfo("�eCommands: �6");
			for (String commands : this.javaPlugin.getDescription().getCommands().keySet()) {
				Console.sendMessageInfo("�6- �e/" + commands);
			}
			Console.sendMessageInfo("�eConsole debug: " + (Console.debug ? "�aYes" : "�cNo"));
			Console.sendMessageInfo("�eRedis connection: �6"
					+ (this.databaseManager.getRedisClient().isConnected() ? "�aYes" : "�cNo"));
			Console.sendMessageInfo("�eMySQL connection: �6"
					+ (this.databaseManager.getMySQLClient().isConnected() ? "�aYes" : "�cNo"));
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�6Machine stats:");
			Console.sendMessageInfo("  �6Memory: �e" + ServerStats.getUsedMemoryRAM() + " �6MB�7/�e"
					+ ServerStats.getMaximumMemoryRAM() + " �6MB");
			try {
				Console.sendMessageInfo("  �6Processor: �e" + ServerStats.getProcessorRead() + "�7/�e"
						+ ServerStats.getProcessorWritten() + " (binary)");
			} catch (Exception e) {
				Console.sendMessageError("  �CThe information recovered have failed ...");
			}
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�aPlugin enabled");
			Console.sendMessageInfo("");
			Console.sendMessageInfo("�7�M----------------------------------------------");
		}
	}

	public void onDisable() {
		if (databaseManager != null) {
			databaseManager.getRedisClient().closeServer();
			databaseManager.deconnectionAllDatabases();
		}
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public Gson getGsonInstance() {
		return this.gsonInstance;
	}

	public CommandsManager getCommandsManager() {
		return this.commandsManager;
	}

	public DatabaseManager getDatabaseManager() {
		return this.databaseManager;
	}

	public Twitter getTwitter() {
		return this.twitter;
	}

	public JavaPlugin getJavaPlugin() {
		return this.javaPlugin;
	}

	public static BloostryPlayer getBloostryPlayer(UUID uuid) {
		if (uuid == null)
			return null;
		if (DatabaseServerType.REDIS.getRequestDatabase().hasData(uuid)) {
			Console.sendMessageDebug("�6BloostryPlayer �eis recovered on Redis Database");
			return DatabaseServerType.REDIS.getRequestDatabase().getData(uuid);
		} else {
			if (DatabaseServerType.MYSQL.getRequestDatabase().hasData(uuid)) {
				Console.sendMessageDebug("�6BloostryPlayer �eis recovered on MySQL Database");
				return DatabaseServerType.MYSQL.getRequestDatabase().getData(uuid);
			} else {
				Console.sendMessageDebug("Player data not found -> " + uuid.toString() + " <- ");
				return null;
			}
		}
	}

	public static BloostryPlayer getBloostryPlayer(String name) {
		if (name == null)
			return null;
		UUID UUID = getUUID(name);
		return getBloostryPlayer(UUID);
	}

	public static UUID getUUID(String name) {
		if (DatabaseServerType.MYSQL.getRequestDatabase().hasUser(name))
			return UUID.fromString(DatabaseServerType.MYSQL.getRequestDatabase().getUUID(name));
		return null;
	}

	public static String getServerName() {
		return Main.javaPlugin.getConfig().getString("server-name");
	}
}

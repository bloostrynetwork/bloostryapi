package mc.bloostrynetwork.antox11200.bloostryapi.player.sanction;

public enum PunishmentType {

	BAN("banni", 3),
	MUTE("mut�e",2),
	KICK("�ject�", 1);
	
	public int punishmentID;
	public String name;
	
	PunishmentType(String name, int punishmentID){
		this.name = name;
		this.punishmentID = punishmentID;
	}
}

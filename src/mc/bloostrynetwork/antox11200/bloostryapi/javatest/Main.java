package mc.bloostrynetwork.antox11200.bloostryapi.javatest;

public class Main {

	public static void main(String[] args) {
		System.out.println(getJsonText().substring(1, getJsonText().length() - 1));
	}

	private static String getJsonText() {
		return "'{\r\n" + 
				"    \"glossary\": {\r\n" + 
				"        \"title\": \"example glossary\",\r\n" + 
				"		\"GlossDiv\": {\r\n" + 
				"            \"title\": \"S\",\r\n" + 
				"			\"GlossList\": {\r\n" + 
				"                \"GlossEntry\": {\r\n" + 
				"                    \"ID\": \"SGML\",\r\n" + 
				"					\"SortAs\": \"SGML\",\r\n" + 
				"					\"GlossTerm\": \"Standard Generalized Markup Language\",\r\n" + 
				"					\"Acronym\": \"SGML\",\r\n" + 
				"					\"Abbrev\": \"ISO 8879:1986\",\r\n" + 
				"					\"GlossDef\": {\r\n" + 
				"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\r\n" + 
				"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\r\n" + 
				"                    },\r\n" + 
				"					\"GlossSee\": \"markup\"\r\n" + 
				"                }\r\n" + 
				"            }\r\n" + 
				"        }\r\n" + 
				"    }\r\n" + 
				"}'";
	}
}
